﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Copo
{
    public partial class Form1 : Form
    {
        bool Quantidade = false;
        bool Liquido = false;
        private Copo meuCopo;
        

        public Form1()
        {
            
            InitializeComponent();
            meuCopo = new Copo();

            meuCopo.Capacidade = meuCopo.Capacidade * 10;

            MensagemLabel.Text = "Por favor escolha a bebida e a quantidade desejada.";
            MLLabel.Text = QuantidadeTrackBar.Value.ToString() + "ML";

            ServirButton.Enabled = false;
            EsvaziarButton.Enabled = false;
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void QuantidadeTrackBar_Scroll(object sender, EventArgs e)
        {
            
            meuCopo.Contem = QuantidadeTrackBar.Value;
            MLLabel.Text = QuantidadeTrackBar.Value.ToString() + "ML";

            if ( QuantidadeTrackBar.Value != 0)
            {
                Quantidade = true;
            }
            else
            {
                Quantidade = false;
            }

            if (Liquido && Quantidade)
            {
                ServirButton.Enabled = true;
            }
            else
            {
                ServirButton.Enabled = false;

            }
        }

        private void AguaButton_Click(object sender, EventArgs e)
        {
            meuCopo.Liquido = "Água";
            Liquido = true;
            if (Liquido && Quantidade)
            {
                ServirButton.Enabled = true;
            }

            AguaButton.BackColor = Color.Blue;
            GinButton.BackColor = default(Color);
            CocaButton.BackColor = default(Color);
            AguaButton.ForeColor = Color.White;
            GinButton.ForeColor = default(Color);
            CocaButton.ForeColor = default(Color);

        }

        private void GinButton_Click(object sender, EventArgs e)
        {
            meuCopo.Liquido = "Gin Tónico";
            Liquido = true;
            if (Liquido && Quantidade)
            {
                ServirButton.Enabled = true;
            }
            GinButton.BackColor = Color.Blue;
            AguaButton.BackColor = default(Color);
            CocaButton.BackColor = default(Color);

            AguaButton.ForeColor = default(Color);
            GinButton.ForeColor = Color.White;
            CocaButton.ForeColor = default(Color);
        }

        private void CocaButton_Click(object sender, EventArgs e)
        {
            meuCopo.Liquido = "Coca-Cola";
            Liquido = true;
            if (Liquido && Quantidade)
            {
                ServirButton.Enabled = true;
            }
            CocaButton.BackColor = Color.Blue;
            AguaButton.BackColor = default(Color);
            GinButton.BackColor = default(Color);

            AguaButton.ForeColor = default(Color);
            GinButton.ForeColor = default(Color);
            CocaButton.ForeColor = Color.White;

        }
        
        private void EsvaziarButton_Click(object sender, EventArgs e)
        {

            
            meuCopo.Esvaziar(meuCopo.Contem);
            EsvaziarButton.Enabled = false;
            ServirButton.Enabled = false;
            CocaButton.Enabled = true;
            GinButton.Enabled = true;
            AguaButton.Enabled = true;
            QuantidadeTrackBar.Enabled = true;
            QuantidadeTrackBar.Value = 0;
            Liquido = false;
            Quantidade = false;
            MensagemLabel.Text = "Por favor escolha a bebida e a quantidade desejada.";
            PercentagemLabel.Text = "";
            AguaButton.BackColor = default(Color);
            GinButton.BackColor = default(Color);
            CocaButton.BackColor = default(Color);
            MLLabel.Text = QuantidadeTrackBar.Value.ToString() + "ML";
            AguaButton.ForeColor = default(Color);
            GinButton.ForeColor = default(Color);
            CocaButton.ForeColor = default(Color);

        }

        private void ServirButton_Click(object sender, EventArgs e)
        {
            CocaButton.Enabled = false;
            GinButton.Enabled = false;
            AguaButton.Enabled = false;
            QuantidadeTrackBar.Enabled = false;

            ServirButton.Enabled = false;
            MensagemLabel.Text = meuCopo.ToString();
            EsvaziarButton.Enabled = true;
            PercentagemLabel.Text = meuCopo.ValorEmPercentagem().ToString() + "% cheio";

        }
    }
}
