﻿namespace Copo
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.QuantidadeTrackBar = new System.Windows.Forms.TrackBar();
            this.CocaButton = new System.Windows.Forms.Button();
            this.GinButton = new System.Windows.Forms.Button();
            this.AguaButton = new System.Windows.Forms.Button();
            this.ServirButton = new System.Windows.Forms.Button();
            this.EsvaziarButton = new System.Windows.Forms.Button();
            this.MensagemLabel = new System.Windows.Forms.Label();
            this.MLLabel = new System.Windows.Forms.Label();
            this.PercentagemLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.QuantidadeTrackBar)).BeginInit();
            this.SuspendLayout();
            // 
            // QuantidadeTrackBar
            // 
            this.QuantidadeTrackBar.Location = new System.Drawing.Point(12, 383);
            this.QuantidadeTrackBar.Maximum = 250;
            this.QuantidadeTrackBar.Name = "QuantidadeTrackBar";
            this.QuantidadeTrackBar.Size = new System.Drawing.Size(374, 45);
            this.QuantidadeTrackBar.TabIndex = 0;
            this.QuantidadeTrackBar.Scroll += new System.EventHandler(this.QuantidadeTrackBar_Scroll);
            // 
            // CocaButton
            // 
            this.CocaButton.Location = new System.Drawing.Point(39, 43);
            this.CocaButton.Name = "CocaButton";
            this.CocaButton.Size = new System.Drawing.Size(153, 141);
            this.CocaButton.TabIndex = 1;
            this.CocaButton.Text = "Coca Cola";
            this.CocaButton.UseVisualStyleBackColor = true;
            this.CocaButton.Click += new System.EventHandler(this.CocaButton_Click);
            // 
            // GinButton
            // 
            this.GinButton.Location = new System.Drawing.Point(209, 43);
            this.GinButton.Name = "GinButton";
            this.GinButton.Size = new System.Drawing.Size(153, 141);
            this.GinButton.TabIndex = 2;
            this.GinButton.Text = "Gin Tónico";
            this.GinButton.UseVisualStyleBackColor = true;
            this.GinButton.Click += new System.EventHandler(this.GinButton_Click);
            // 
            // AguaButton
            // 
            this.AguaButton.Location = new System.Drawing.Point(122, 201);
            this.AguaButton.Name = "AguaButton";
            this.AguaButton.Size = new System.Drawing.Size(153, 141);
            this.AguaButton.TabIndex = 3;
            this.AguaButton.Text = "Água";
            this.AguaButton.UseVisualStyleBackColor = true;
            this.AguaButton.Click += new System.EventHandler(this.AguaButton_Click);
            // 
            // ServirButton
            // 
            this.ServirButton.Location = new System.Drawing.Point(52, 455);
            this.ServirButton.Name = "ServirButton";
            this.ServirButton.Size = new System.Drawing.Size(140, 79);
            this.ServirButton.TabIndex = 4;
            this.ServirButton.Text = "Servir";
            this.ServirButton.UseVisualStyleBackColor = true;
            this.ServirButton.Click += new System.EventHandler(this.ServirButton_Click);
            // 
            // EsvaziarButton
            // 
            this.EsvaziarButton.Location = new System.Drawing.Point(209, 455);
            this.EsvaziarButton.Name = "EsvaziarButton";
            this.EsvaziarButton.Size = new System.Drawing.Size(140, 79);
            this.EsvaziarButton.TabIndex = 5;
            this.EsvaziarButton.Text = "Esvaziar";
            this.EsvaziarButton.UseVisualStyleBackColor = true;
            this.EsvaziarButton.Click += new System.EventHandler(this.EsvaziarButton_Click);
            // 
            // MensagemLabel
            // 
            this.MensagemLabel.AutoSize = true;
            this.MensagemLabel.Location = new System.Drawing.Point(68, 568);
            this.MensagemLabel.Name = "MensagemLabel";
            this.MensagemLabel.Size = new System.Drawing.Size(0, 13);
            this.MensagemLabel.TabIndex = 6;
            // 
            // MLLabel
            // 
            this.MLLabel.AutoSize = true;
            this.MLLabel.Location = new System.Drawing.Point(180, 367);
            this.MLLabel.Name = "MLLabel";
            this.MLLabel.Size = new System.Drawing.Size(22, 13);
            this.MLLabel.TabIndex = 7;
            this.MLLabel.Text = "ML";
            // 
            // PercentagemLabel
            // 
            this.PercentagemLabel.AutoSize = true;
            this.PercentagemLabel.Location = new System.Drawing.Point(209, 541);
            this.PercentagemLabel.Name = "PercentagemLabel";
            this.PercentagemLabel.Size = new System.Drawing.Size(0, 13);
            this.PercentagemLabel.TabIndex = 8;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(399, 605);
            this.Controls.Add(this.PercentagemLabel);
            this.Controls.Add(this.MLLabel);
            this.Controls.Add(this.MensagemLabel);
            this.Controls.Add(this.EsvaziarButton);
            this.Controls.Add(this.ServirButton);
            this.Controls.Add(this.AguaButton);
            this.Controls.Add(this.GinButton);
            this.Controls.Add(this.CocaButton);
            this.Controls.Add(this.QuantidadeTrackBar);
            this.Name = "Form1";
            this.Text = "QuantidadeTrackBar";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.QuantidadeTrackBar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TrackBar QuantidadeTrackBar;
        private System.Windows.Forms.Button CocaButton;
        private System.Windows.Forms.Button GinButton;
        private System.Windows.Forms.Button AguaButton;
        private System.Windows.Forms.Button ServirButton;
        private System.Windows.Forms.Button EsvaziarButton;
        private System.Windows.Forms.Label MensagemLabel;
        private System.Windows.Forms.Label MLLabel;
        private System.Windows.Forms.Label PercentagemLabel;
    }
}

